import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';


function AttendeesList() {
  const { conferenceId } = useParams();
  const [data, setData] = useState(null);

  const navigate = useNavigate();

  function handleEdit(attendeeId) {
    navigate(`/attendees/edit/${attendeeId}/`);
  }

  const handleDelete = async (conferenceId, attendeeId) => {
    try {
      // Perform the delete operation
      const deleteUrl = `http://localhost:8001/api/attendees/${attendeeId}/`;
      console.log(deleteUrl);
      await fetch(deleteUrl, { method: 'DELETE' });

      const fetchData = async () => {
        try {
          const url = `http://localhost:8001/api/conferences/${conferenceId}/attendees/`;
          const response = await fetch(url);
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          const responseData = await response.json();
          setData(responseData);
        } catch (error) {
          console.error('Error fetching data:', error);
        }
      };

      fetchData();
    } catch (error) {
      console.error('Error deleting attendee:', error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const url = `http://localhost:8001/api/conferences/${conferenceId}/attendees/`;
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        const responseData = await response.json();
        setData(responseData);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [conferenceId, setData]);

  return (
    <div className="container">
      <p>Conference ID: {conferenceId}</p>
      {data && data.attendees ? (
        <div className="row justify-content-center">
          <div className="col-md-6">
            <table className="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                {data.attendees.map((attendee, index) => (
                  <tr key={attendee.id || index}>
                    <td>{attendee.name}</td>
                    <td>
                      <button
                        className="btn btn-primary"
                        onClick={() => handleEdit(attendee.id)}
                      >Edit</button>
                    </td>
                    <td>
                      <button
                        className="btn btn-danger"
                        onClick={() => handleDelete(conferenceId, attendee.id)}
                      >Delete</button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default AttendeesList;
