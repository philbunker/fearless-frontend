import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function EditAttendee() {
  const { attendeeId } = useParams();
  const [attendeeData, setAttendeeData] = useState(null);
  const [conferences, setConferences] = useState([])

  useEffect(() => {
    const fetchDataAttendee = async () => {
        try {
          const attendeeUrl = `http://localhost:8001/api/attendees/${attendeeId}/`;
          const attendeeResponse = await fetch(attendeeUrl);
          if (!attendeeResponse.ok) {
            throw new Error(`HTTP error! Status: ${attendeeResponse.status}`);
          }
          const attendeeResponseData = await attendeeResponse.json();
          setAttendeeData(attendeeResponseData);
          console.log(attendeeResponseData.has_account);
          console.log(attendeeResponseData.conference.name);
        } catch (error) {
          console.error('Error fetching attendee data:', error);
        }
      };

    const fetchDataConferences = async () => {
        try {
            const conferencesUrl = `http://localhost:8000/api/conferences/`;
            const conferencesResponse = await fetch(conferencesUrl);
            if (!conferencesResponse.ok) {
            throw new Error(`HTTP error! Status: ${conferencesResponse.status}`);
            }
            const conferencesResponseData = await conferencesResponse.json();
            setConferences(conferencesResponseData.conferences);
            // console.log(conferencesResponseData);
        } catch (error) {
            console.error('Error fetching attendee data:', error);
        }
    };

    fetchDataAttendee();
    fetchDataConferences();
  }, [attendeeId]);


  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Edit Attendee</h1>
                {attendeeData && (
                <form id="create-presentation-form">
                    <div className="form-floating mb-3">
                        <label htmlFor="name">Name:</label>
                        <input placeholder="Name" required type="text" id="name" name="name" className="form-control" value={attendeeData.name} />
                    </div>
                    <div className="form-floating mb-3">
                        <label htmlFor="email">Email</label>
                        <input placeholder="Email" required type="text" id="email" name="email" value={attendeeData.email} className="form-control" />
                    </div>
                    <div className="form-floating mb-3">
                        <label htmlFor="company_name">Company name</label>
                        <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                    </div>
                    <div className="mb-3">
                        <select required
                            value={attendeeData.conference.id}
                            onChange={(e) => setAttendeeData({...attendeeData, conference: { ...attendeeData.conference, id: e.target.value }})}
                            name="conference" id="conference" className="form-select">
                            <option defaultValue value="">Choose a conference</option>
                            {conferences.map(conference => {
                                return (
                                    <option key={conference.id} value={conference.id}>{conference.name}</option>
                                )
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <select required
                            value={attendeeData.has_account}
                            onChange={(e) => setAttendeeData({ ...attendeeData, has_account: e.target.value === 'true' })}
                            name="hasAccount" id="hasAccount" className="form-select">
                            <option value="">Has account</option>
                            <option value="true">True</option>
                            <option value="false">False</option>
                        </select>
                    </div>
                    <button className="btn btn-primary">Save Changes</button>
                </form>
                )}
            </div>
        </div>
    </div>
  );
}

export default EditAttendee;
