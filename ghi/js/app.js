

function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
      <div class="card shadow-lg p-3 mb-5 bg-body-tertiary rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          ${startDate} - ${endDate}
        </div>
      </div>
    `;
}

function displayError(e) {
    const row = document.querySelector('.row');
    row.innerHTML += `
        <div class="alert alert-danger" role="alert">
        Error: ${e}
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    // const url = 'https://jsonplaceholder.typicode.com/posts/404';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            displayError(response.status);
        } else {
            const data = await response.json();

            let confTotal = 0;
            let confCol = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts)
                    const startDateFormatted = startDate.toLocaleDateString('en-US');
                    const endDate = new Date(details.conference.ends)
                    const endDateFormatted = endDate.toLocaleDateString('en-US');
                    const locationName = details.conference.location.name;

                    const html = createCard(title, description, pictureUrl, startDateFormatted, endDateFormatted, locationName);
                    confCol = (confTotal++ % 3) + 1;
                    const column = document.querySelector(`#col${confCol}`);
                    column.innerHTML += html;
                }
            }
        }
    } catch (e) {
        displayError(e);
    }
});
